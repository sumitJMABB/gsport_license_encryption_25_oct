﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Microsoft.Win32;

namespace WebComCation
{
    public partial class frmReleaseLicense : Form
    {
        ServerOutInMsg msgSrvr;
        Registry_Handler reg;
        public frmReleaseLicense()
        {
            InitializeComponent();
        }

        private void frmReleaseLicense_Load(object sender, EventArgs e)
        {
            try
            {
                reg = new Registry_Handler();
                msgSrvr = reg.GetStoredRegistryMsg();
                txtLicense.Text = msgSrvr.LicenseKey;
                if (msgSrvr.LicenseKey.Trim() == "")
                    btnRelease.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"GSPORT");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string ReleaseLicense()
        {
            string retMsg = "License released successfully From this system.";
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string jsonQuery = "[{\"Computer_id\":\"" + keyRequest.GetComputerID() +
                                    "\",\"Computer_name\":\"" + keyRequest.GetComputerName() +
                                    "\",\"Activation_key\":\"" + txtLicense.Text + "\"}]";
                String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
                var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
                //3.
                JsonSerializer jsonSerializer = new JsonSerializer();
                //4.
                MemoryStream objBsonMemoryStream = new MemoryStream();
                //5.
                Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
                //6.
                jsonSerializer.Serialize(bsonWriterObject, studentObject);
                // text = queryString;
                byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);



                #region WebRequest

                //Connect to our Yugamiru Web Server//http://gs-demo.jma.website/apioauthdata/index.php/home/releasedata (url for release)
                //WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/activatekeydata");
                WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/releasedata");
                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = requestByte.Length;
                Stream webDataStream = webRequest.GetRequestStream();
                webDataStream.Write(requestByte, 0, requestByte.Length);

                string ed = webDataStream.ToString();

                // get the response from our stream

                WebResponse webResponse = webRequest.GetResponse();
                webDataStream = webResponse.GetResponseStream();

                // convert the result into a String
                StreamReader webResponseSReader = new StreamReader(webDataStream);
                String responseFromServer = webResponseSReader.ReadToEnd();
                #endregion
                //clear registry
                RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\WinRegistry");
                key.SetValue("LicenseKey", "");
                key.SetValue("ComputerName", "");
                key.SetValue("ComputerID", "");
                key.SetValue("Status", "");
                key.SetValue("ServerMessage", "");
                key.SetValue("StartDate", "");
                key.SetValue("EndDate", "");
                key.SetValue("ActivationDate", "");
                key.SetValue("LastRunDate", "");//Utility.DateTimeToString(DateTime.Now));

                try
                {
                    key.Close();
                }
                catch
                { }
                btnRelease.Enabled = false;
                btnCancel.Text = "Close";
            }
            catch(Exception ex)
            {

                retMsg = "Cannot release this key"+Environment.NewLine+ ex.Message;
                //btnRelease.Text = "Cancel";
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
            return retMsg;
        }

        private void btnRelease_Click(object sender, EventArgs e)
        {
            MessageBox.Show(ReleaseLicense(),"GSPORT");
            this.Close();
            Application.Exit();
            //Environment.Exit(1);
        }
    }
}
