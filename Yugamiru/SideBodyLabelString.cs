﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Yugamiru
{
    public class SideBodyLabelString
    {
        SideBodyPosition SideBodyPosition;
		SideBodyAngle SideBodyAngle;
        int iBenchmarkDistance;     
	    string m_strEarPosition;
        string m_strShoulderPosition;
        string m_strHipPosition;
        string m_strKneePosition;
        string m_strAnklePosition;
        string m_strHipBalance;
        string m_strHipShoulderAngle;
        string m_strShoulderEarAngle;

        SideBodyLabelString()
        {

            m_strEarPosition = string.Empty;

            m_strShoulderPosition = string.Empty;

            m_strHipPosition = string.Empty;

            m_strKneePosition = string.Empty;

            m_strAnklePosition = string.Empty;

            m_strHipBalance = string.Empty;

            m_strHipShoulderAngle = string.Empty;

            m_strShoulderEarAngle = string.Empty;

        }

        public SideBodyLabelString(
		ref SideBodyPosition SideBodyPosition,
		ref SideBodyAngle SideBodyAngle,
		int iBenchmarkDistance )
        {
            m_strEarPosition = string.Empty;

            m_strShoulderPosition = string.Empty;

            m_strHipPosition = string.Empty;

            m_strKneePosition = string.Empty;

            m_strAnklePosition = string.Empty;

            m_strHipBalance = string.Empty;

            m_strHipShoulderAngle = string.Empty;

            m_strShoulderEarAngle = string.Empty;

            SetData(ref SideBodyPosition, ref SideBodyAngle, iBenchmarkDistance);
        }

       

    void SetData( 
		ref SideBodyPosition SideBodyPosition,
    	ref SideBodyAngle SideBodyAngle,
            int iBenchmarkDistance)
    {
        Point ptBenchmark1 = new Point();
        Point ptBenchmark2 = new Point();
        SideBodyPosition.GetBenchmark1Position(ref ptBenchmark1);
        SideBodyPosition.GetBenchmark2Position(ref ptBenchmark2);

            var X = ptBenchmark2.X - ptBenchmark1.X;
            var Y = ptBenchmark2.Y - ptBenchmark1.Y;
            Point ptBenchmarkDistance = new Point(X, Y);

            int iPixelDistanceSquare = ptBenchmarkDistance.X * ptBenchmarkDistance.X+ ptBenchmarkDistance.Y * ptBenchmarkDistance.Y;
        if (iPixelDistanceSquare <= 0)
        {
            iPixelDistanceSquare = 1;
        }
        double dPixelDistance =Math.Sqrt((double)iPixelDistanceSquare);

        double dDistancePerPixel = iBenchmarkDistance / dPixelDistance;

        double dAnklePixelPos = SideBodyPosition.GetAnkleX();
        double dEarPixelOffset = SideBodyPosition.GetEarX() - dAnklePixelPos;
        double dShoulderPixelOffset = SideBodyPosition.GetShoulderX() - dAnklePixelPos;
        double dHipPixelOffset = SideBodyPosition.GetHipX() - dAnklePixelPos;
        double dKneePixelOffset = SideBodyPosition.GetKneeX() - dAnklePixelPos;
        double dAnklePixelOffset = SideBodyPosition.GetAnkleX() - dAnklePixelPos;

        double dEarCentimeterOffset = (dEarPixelOffset * dDistancePerPixel);
        double dShoulderCentimeterOffset = (dShoulderPixelOffset * dDistancePerPixel);
        double dHipCentimeterOffset = (dHipPixelOffset * dDistancePerPixel);
        double dKneeCentimeterOffset = (dKneePixelOffset * dDistancePerPixel);
        double dAnkleCentimeterOffset = (dAnklePixelOffset * dDistancePerPixel);

        if (dEarCentimeterOffset < 0)
        { 
            m_strEarPosition = (-dEarCentimeterOffset).ToString("0.0") + "cm right";
        }
        else if (dEarCentimeterOffset > 0)
        {
            m_strEarPosition = (-dEarCentimeterOffset).ToString("0.0") + "cm left";
            }
        else
        {
            m_strEarPosition = "0cm";
        }

        if (dShoulderCentimeterOffset < 0)
        {
            m_strShoulderPosition = (-dShoulderCentimeterOffset).ToString("0.0") + "cm right";
        }
        else if (dShoulderCentimeterOffset > 0)
        {
            m_strShoulderPosition = (-dShoulderCentimeterOffset).ToString("0.0") + "cm left";
        }
        else
        {
            m_strShoulderPosition = "0cm";
        }

        if (dHipCentimeterOffset < 0)
        {
            m_strHipPosition = (-dHipCentimeterOffset).ToString("0.0") + "cm right";
        }
        else if (dHipCentimeterOffset > 0)
        {
                m_strHipPosition = (-dHipCentimeterOffset).ToString("0.0") + "cm left";
        }
        else
        {
            m_strHipPosition = "0cm";
        }

        if (dKneeCentimeterOffset < 0)
        {
            m_strKneePosition = (-dKneeCentimeterOffset).ToString("0.0") + "cm right";
        }
        else if (dKneeCentimeterOffset > 0)
        {
            m_strKneePosition = (+dKneeCentimeterOffset).ToString("0.0") + "cm left";
        }
        else
        {
            m_strKneePosition = "0cm";
        }

        if (dAnkleCentimeterOffset < 0)
        {
            m_strAnklePosition = (-dAnkleCentimeterOffset).ToString("0.0") + "cm right";
        }
        else if (dAnkleCentimeterOffset > 0)
        {
            m_strAnklePosition = (+dAnkleCentimeterOffset).ToString("0.0") + "cm left";
        }
        else
        {
            m_strAnklePosition = "0cm";
        }
        m_strAnklePosition = "";

            
            m_strHipBalance = SideBodyAngle.GetPelvicAngle().ToString("0.0") + Yugamiru.Properties.Resources.DEG;
        m_strHipShoulderAngle = SideBodyAngle.GetBodyBalance().ToString("0.0") + Yugamiru.Properties.Resources.DEG;
            m_strShoulderEarAngle = SideBodyAngle.GetShoulderEarAngle().ToString("0.0") + Yugamiru.Properties.Resources.DEG;
        }

  public  string GetEarPositionString() 
{
	return m_strEarPosition;
}

        public string GetShoulderPositionString() 
{
	return m_strShoulderPosition;
}

        public string GetHipPositionString() 
{
	return m_strHipPosition;
}

        public string GetKneePositionString() 
{
	return m_strKneePosition;
}

        public string GetAnklePositionString() 
{
	return m_strAnklePosition;
}

        public string GetHipBalanceString() 
{
	return m_strHipBalance;
}

        public string GetHipShoulderAngleString() 
{
	return	m_strHipShoulderAngle;
}

        public string GetShoulderEarAngleString() 
{
	return	m_strShoulderEarAngle;
}


    }
}
