﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            /*IDD_BALANCELABO_DIALOG StartingForm = new IDD_BALANCELABO_DIALOG();
            StartingForm.Text = Yugamiru.Properties.Resources.YUGAMIRU_TITLE;
            Application.Run(StartingForm); Commented by meena becoz of out of memory exception*/ // this is the main page
            Application.Run(new IDD_BALANCELABO_DIALOG());
            //Application.Run(new JointEditView());
            //Application.Run(new Form4());


        }
    }
}
