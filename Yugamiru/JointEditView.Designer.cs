﻿namespace Yugamiru
{
    partial class JointEditView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox_standing = new System.Windows.Forms.PictureBox();
            this.pictureBox_KneeDown = new System.Windows.Forms.PictureBox();
            this.IDC_Mag2Btn = new System.Windows.Forms.PictureBox();
            this.IDC_Mag1Btn = new System.Windows.Forms.PictureBox();
            this.IDC_CancelBtn = new System.Windows.Forms.PictureBox();
            this.IDC_OkBtn = new System.Windows.Forms.PictureBox();
            this.IDC_ResetImgBtn = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.IDC_SLIDER1 = new System.Windows.Forms.TrackBar();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_standing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_KneeDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Mag2Btn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Mag1Btn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_CancelBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_OkBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_ResetImgBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_SLIDER1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox_standing
            // 
            this.pictureBox_standing.Location = new System.Drawing.Point(27, 12);
            this.pictureBox_standing.Name = "pictureBox_standing";
            this.pictureBox_standing.Size = new System.Drawing.Size(58, 77);
            this.pictureBox_standing.TabIndex = 0;
            this.pictureBox_standing.TabStop = false;
            this.pictureBox_standing.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox_standing.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox_standing.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox_standing.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // pictureBox_KneeDown
            // 
            this.pictureBox_KneeDown.Location = new System.Drawing.Point(115, 12);
            this.pictureBox_KneeDown.Name = "pictureBox_KneeDown";
            this.pictureBox_KneeDown.Size = new System.Drawing.Size(58, 77);
            this.pictureBox_KneeDown.TabIndex = 1;
            this.pictureBox_KneeDown.TabStop = false;
            this.pictureBox_KneeDown.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_KneeDown_Paint);
            this.pictureBox_KneeDown.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_KneeDown_MouseDown);
            this.pictureBox_KneeDown.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_KneeDown_MouseMove);
            this.pictureBox_KneeDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_KneeDown_MouseUp);
            // 
            // IDC_Mag2Btn
            // 
            this.IDC_Mag2Btn.Location = new System.Drawing.Point(352, 39);
            this.IDC_Mag2Btn.Name = "IDC_Mag2Btn";
            this.IDC_Mag2Btn.Size = new System.Drawing.Size(36, 35);
            this.IDC_Mag2Btn.TabIndex = 2;
            this.IDC_Mag2Btn.TabStop = false;
            this.IDC_Mag2Btn.Click += new System.EventHandler(this.IDC_Mag2Btn_Click);
            // 
            // IDC_Mag1Btn
            // 
            this.IDC_Mag1Btn.Location = new System.Drawing.Point(352, 190);
            this.IDC_Mag1Btn.Name = "IDC_Mag1Btn";
            this.IDC_Mag1Btn.Size = new System.Drawing.Size(36, 35);
            this.IDC_Mag1Btn.TabIndex = 3;
            this.IDC_Mag1Btn.TabStop = false;
            this.IDC_Mag1Btn.Click += new System.EventHandler(this.IDC_Mag1Btn_Click);
            // 
            // IDC_CancelBtn
            // 
            this.IDC_CancelBtn.Location = new System.Drawing.Point(27, 264);
            this.IDC_CancelBtn.Name = "IDC_CancelBtn";
            this.IDC_CancelBtn.Size = new System.Drawing.Size(83, 48);
            this.IDC_CancelBtn.TabIndex = 5;
            this.IDC_CancelBtn.TabStop = false;
            this.IDC_CancelBtn.Click += new System.EventHandler(this.IDC_CancelBtn_Click);
            // 
            // IDC_OkBtn
            // 
            this.IDC_OkBtn.Location = new System.Drawing.Point(219, 264);
            this.IDC_OkBtn.Name = "IDC_OkBtn";
            this.IDC_OkBtn.Size = new System.Drawing.Size(83, 48);
            this.IDC_OkBtn.TabIndex = 6;
            this.IDC_OkBtn.TabStop = false;
            this.IDC_OkBtn.Click += new System.EventHandler(this.IDC_OkBtn_Click);
            // 
            // IDC_ResetImgBtn
            // 
            this.IDC_ResetImgBtn.Location = new System.Drawing.Point(352, 235);
            this.IDC_ResetImgBtn.Name = "IDC_ResetImgBtn";
            this.IDC_ResetImgBtn.Size = new System.Drawing.Size(36, 35);
            this.IDC_ResetImgBtn.TabIndex = 7;
            this.IDC_ResetImgBtn.TabStop = false;
            this.IDC_ResetImgBtn.Click += new System.EventHandler(this.IDC_ResetImgBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 146);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "label2";
            // 
            // IDC_SLIDER1
            // 
            this.IDC_SLIDER1.BackColor = System.Drawing.Color.White;
            this.IDC_SLIDER1.Location = new System.Drawing.Point(-2, -2);
            this.IDC_SLIDER1.Margin = new System.Windows.Forms.Padding(1);
            this.IDC_SLIDER1.Name = "IDC_SLIDER1";
            this.IDC_SLIDER1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.IDC_SLIDER1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.IDC_SLIDER1.RightToLeftLayout = true;
            this.IDC_SLIDER1.Size = new System.Drawing.Size(45, 115);
            this.IDC_SLIDER1.TabIndex = 10;
            this.IDC_SLIDER1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.IDC_SLIDER1.Scroll += new System.EventHandler(this.IDC_SLIDER1_Scroll);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.IDC_SLIDER1);
            this.panel1.Location = new System.Drawing.Point(409, 76);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(45, 118);
            this.panel1.TabIndex = 11;
            // 
            // JointEditView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(600, 661);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.IDC_ResetImgBtn);
            this.Controls.Add(this.IDC_OkBtn);
            this.Controls.Add(this.IDC_CancelBtn);
            this.Controls.Add(this.IDC_Mag1Btn);
            this.Controls.Add(this.IDC_Mag2Btn);
            this.Controls.Add(this.pictureBox_KneeDown);
            this.Controls.Add(this.pictureBox_standing);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "JointEditView";
            this.Text = "JointEditView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.JointEditView_FormClosing);
            this.Load += new System.EventHandler(this.JointEditView_Load);
            this.SizeChanged += new System.EventHandler(this.JointEditView_SizeChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.JointEditView_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_standing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_KneeDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Mag2Btn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Mag1Btn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_CancelBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_OkBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_ResetImgBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_SLIDER1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_standing;
        private System.Windows.Forms.PictureBox pictureBox_KneeDown;
        private System.Windows.Forms.PictureBox IDC_Mag2Btn;
        private System.Windows.Forms.PictureBox IDC_Mag1Btn;
        private System.Windows.Forms.PictureBox IDC_CancelBtn;
        private System.Windows.Forms.PictureBox IDC_OkBtn;
        private System.Windows.Forms.PictureBox IDC_ResetImgBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar IDC_SLIDER1;
        private System.Windows.Forms.Panel panel1;
    }
}