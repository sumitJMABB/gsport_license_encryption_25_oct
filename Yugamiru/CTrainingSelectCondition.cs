﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class CTrainingSelectCondition
    {
        public int m_iConditionID;
        public int m_iPriority;

        public int Check(int iPriority, ResultData result)
        {

            iPriority = 0;
            switch (m_iConditionID)
            {
                case Constants.TRAINING_SELECTCONDITIONID_NONE:
                    return 0;
                case Constants.TRAINING_SELECTCONDITIONID_BAD_SHOULDERBAL:
                    //SHOULDERBAL‚Ì’l‚ªˆ«‚¢(Œ¨ƒVƒtƒgj
                    if ((result.GetStandingShoulderBal() != 0) || (result.GetKneedownShoulderBal() != 0))
                    {
                        iPriority = m_iPriority;
                        return 1;
                    }
                    break;
                case Constants.TRAINING_SELECTCONDITIONID_BAD_HEADCENTER:
                    //HEADCENTER‚Ì’l‚ªˆ«‚¢iŽñƒVƒtƒgj
                    if ((result.GetStandingHeadCenter() != 0) || (result.GetKneedownHeadCenter() != 0))
                    {
                        iPriority = m_iPriority;
                        return 1;
                    }
                    break;
                case Constants.TRAINING_SELECTCONDITIONID_BAD_HIP:
                    //HIP‚Ì’l‚ªˆ«‚¢iœ”Õ‰ñùj
                    if ((result.GetStandingHip() != 0) || (result.GetKneedownHip() != 0))
                    {
                        iPriority = m_iPriority;
                        return 1;
                    }
                    break;
                case Constants.TRAINING_SELECTCONDITIONID_BAD_CENTERBALANCE:
                    //CENTERBALANCE‚Ì’l‚ªˆ«‚¢iœ”ÕƒVƒtƒgj
                    if ((result.GetStandingCenterBalance() != 0) || (result.GetKneedownCenterBalance() != 0))
                    {
                        iPriority = m_iPriority;
                        return 1;
                    }
                    break;
                case Constants.TRAINING_SELECTCONDITIONID_XLEGGED:
                    //X‹r
                    if ((result.GetStandingLeftKnee() < 0) || (result.GetStandingRightKnee() < 0) ||
                        (result.GetKneedownLeftKnee() < 0) || (result.GetKneedownRightKnee() < 0))
                    {
                        iPriority = m_iPriority;
                        return 1;
                    }
                    break;
                case Constants.TRAINING_SELECTCONDITIONID_OLEGGED:
                    //O‹r
                    if ((result.GetStandingLeftKnee() > 0) || (result.GetStandingRightKnee() > 0) ||
                        (result.GetKneedownLeftKnee() > 0) || (result.GetKneedownRightKnee() > 0))
                    {
                        iPriority = m_iPriority;
                        return 1;
                    }
                    break;
                case Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_STOOP_AND_BEND_BACKWARD:
                    /* ”L”w{”½‚è˜ */
                    if (result.GetPosturePatternID() == Constants.POSTUREPATTERNID_STOOP_AND_BEND_BACKWARD)
                    {
                        iPriority = m_iPriority;
                        return 1;
                    }
                    break;
                case Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_STOOP:
                    /* ”L”w */
                    if (result.GetPosturePatternID() == Constants.POSTUREPATTERNID_STOOP)
                    {
                        iPriority = m_iPriority;
                        return 1;
                    }
                    break;
                case Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_BEND_BACKWARD:
                    /* ”½‚è˜ */
                    if (result.GetPosturePatternID() == Constants.POSTUREPATTERNID_BEND_BACKWARD)
                    {
                        iPriority = m_iPriority;
                        return 1;
                    }
                    break;
                case Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_FLATBACK:
                    /* ƒtƒ‰ƒbƒgƒoƒbƒN */
                    if (result.GetPosturePatternID() == Constants.POSTUREPATTERNID_FLATBACK)
                    {
                        iPriority = m_iPriority;
                        return 1;
                    }
                    break;
                case Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_NORMAL:
                    /* —‘z‚ÌŽp¨ */
                    if (result.GetPosturePatternID() == Constants.POSTUREPATTERNID_NORMAL)
                    {
                        iPriority = m_iPriority;
                        return 1;
                    }
                    break;
                case Constants.TRAINING_SELECTCONDITIONID_POSTUREPATTERN_FRONTSIDE_UNBALANCED:
                    /* ³–Êƒoƒ‰ƒ“ƒX”ñ‘ÎÌ */
                    if (result.GetPosturePatternID() == Constants.POSTUREPATTERNID_FRONTSIDE_UNBALANCED)
                    {
                        iPriority = m_iPriority;
                        return 1;
                    }
                    break;
                default:
                    break;
            }
            return 0;
        }


    }
}
